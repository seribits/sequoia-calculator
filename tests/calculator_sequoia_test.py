# -*- coding: utf-8 -*-
"""Test Unitarios para calculadora SEQUOIA."""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from unittest import TestCase

from sequoia_calculator.sequoia import SEQUOIA


"""Run test.

$ python -m unittest discover -v tests -p "*_test.py"
"""


class TestCalculatorSEQUOIA(TestCase):
    """Test Calculator SEQUOIA."""
    VERSION = '0.1.3'
    DEFAULT_MODE = 1
    MODES = [1, 2]
    NAME = 'SEQUOIA'

    def setUp(self):
        self.calculator = SEQUOIA(self.DEFAULT_MODE)

    def tearDown(self):
        self.calculator = None

    def test_version(self):
        self.assertEqual(self.calculator.get_version(), self.VERSION)

    def test_name(self):
        self.assertEqual(self.calculator.get_name(), self.NAME)

    def test_validate_frequency_true(self):
        """Validate frequency True.

        :attention: `subTest` - Not support python < 3.4
        """
        freq_data = [85, 87, 89, 98, 110, 115]
        for i in freq_data:
            # Context accuracy for the error within the subtest i=i or i
            with self.subTest(i=i):
                self.assertTrue(self.calculator.validate_frequency(i))

    def test_validate_frequency_false(self):
        """Validate frequency True.
        :attention: `subTest` - Not support python < 3.4
        """
        freq_data = [0, 31, 84, 116, 129, 130, 135]
        for i in freq_data:
            # Context accuracy for the error within the subtest i=i or i
            with self.subTest(i=i):
                self.assertFalse(self.calculator.validate_frequency(i))

    def test_calculator_mk(self):
        """RMS expected in given time, units mK."""
        central_frequency = 100  # GHz (85 - 115)
        spectral_mode = {'b': 200, 'ch': 8192}  # dict
        mapx = 1800  # arcsec
        mapy = 1800  # arcsec
        scan_speed = 60  # arcsec/sec
        time = 90  # min
        units = 'mK'
        result = ([1, 43.12, 272.98], [2, 86.25, 193.03], [3, 129.37, 157.61])  # tuple
        self.calculator.set_mode(1)
        self.assertEqual(self.calculator.calculate(
            central_frequency=central_frequency,
            spectral_mode=spectral_mode,
            mapx=mapx, mapy=mapy,
            scan_speed=scan_speed,
            time=time,
            units=units
        ), result)

    def test_calculator_mjy(self):
        """RMS expected in given time, units mJy."""
        central_frequency = 100  # GHz (85 - 115)
        spectral_mode = {'b': 200, 'ch': 8192}  # dict
        mapx = 1800  # arcsec
        mapy = 1800  # arcsec
        scan_speed = 60  # arcsec/sec
        time = 90  # min
        units = 'mJy'
        result = ([1, 43.12, 873.54], [2, 86.25, 617.69], [3, 129.37, 504.34])  # tuple
        self.calculator.set_mode(1)
        self.assertEqual(self.calculator.calculate(
            central_frequency=central_frequency,
            spectral_mode=spectral_mode,
            mapx=mapx, mapy=mapy,
            scan_speed=scan_speed,
            time=time,
            units=units
        ), result)

    def test_calculator_temperature(self):
        """Time required for target RMS, units Temperature."""
        spectral_mode = {'b': 200, 'ch': 8192}  # dict
        central_frequency = 100  # GHz
        mapx = 1800  # arcsec
        mapy = 1800  # arcsec
        scan_speed = 60  # arcsec/sec
        rms = 193.03  # mK
        units = 'Temperature'
        result = ([1, 43.12, 272.98], [2, 86.25, 193.03])  # tuple
        self.calculator.set_mode(2)
        self.assertEqual(self.calculator.calculate(
            central_frequency=central_frequency,
            spectral_mode=spectral_mode,
            mapx=mapx, mapy=mapy,
            scan_speed=scan_speed,
            rms=rms,
            units=units
        ), result)

    def test_calculator_flux(self):
        """Time required for target RMS, units Flux."""
        spectral_mode = {'b': 200, 'ch': 8192}  # dict
        central_frequency = 100  # GHz
        mapx = 1800  # arcsec
        mapy = 1800  # arcsec
        scan_speed = 60  # arcsec/sec
        rms = 617.69  # mJy
        units = 'Flux'
        result = ([1, 43.12, 873.54], [2, 86.25, 617.69])  # tuple
        self.calculator.set_mode(2)
        self.assertEqual(self.calculator.calculate(
            central_frequency=central_frequency,
            spectral_mode=spectral_mode,
            mapx=mapx, mapy=mapy,
            scan_speed=scan_speed,
            rms=rms,
            units=units
        ), result)
