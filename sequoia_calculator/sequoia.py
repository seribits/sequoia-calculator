# -*- coding: utf-8 -*-
"""Calculadora SEQUOIA.

Este modulo nos permite realizar los siguientes calculos:
1. RMS expected in given time. [mK, mJy].
2. Time required for target RMS [Temperature, Flux].
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from math import ceil, floor, sqrt

from .version import version


class SEQUOIA(object):
    """Clase de la calculadora SEQUOIA."""
    T_SYS = 100  # k
    FR = 0.9
    TTF = 3.2
    NAME = 'SEQUOIA'

    def __init__(self, mode=None):
        """Constructor de la clase.

        :Parameters:

        - 'mode data' (int) - Modo de la calculadora (1 o 2)
        """

        self.__mode = mode
        self.__name = self.NAME
        self.__kwargs = None

    def get_version(self):
        """Versión de la calculadora.

        :rtype: str
        """
        return version

    def get_name(self):
        """Nombre de la calculadora.

        :rtype: str
        """
        return self.__name

    def set_mode(self, mode):
        """Agrega el modo de la calculadora.

        :Parameters:

        - 'mode' (int) - Modo de la calculadora (1 o 2)

        """
        self.__mode = mode

    def validate_frequency(self, freq):
        """Validación de 'frequency'.

        :Parameters:

        - 'freq' (float) - Frecuencia dado en GHz.

        :return: True si el valor va de 85 a 115

        :rtype: bool

        """
        return True if freq >= 85 and freq <= 115 else False

    def __calculations(self):
        """Operaciones comunes.

        :rtype: tuple
        """
        barion_lambda = 299.793 / self.__kwargs['central_frequency']
        spectral_mode = self.__kwargs['spectral_mode']
        band = spectral_mode['b'] / spectral_mode['ch']
        nmap = (self.__kwargs['mapx'] + 120) * (self.__kwargs['mapy']) / ((2.06265 * barion_lambda) ** 2)
        map_time = (nmap * 2.06265 * barion_lambda) / (self.FR * self.__kwargs['scan_speed'] * 14400)
        map_rms = (self.T_SYS / (8.3138 * (10 ** 5))) * sqrt(nmap / (band * map_time))
        return (map_time, map_rms)

    def __calculate_mk(self):
        """Obtención de datos para el modo 1 y unidad mK.

        :rtype: tuple
        """
        map_time, map_rms = self.__calculations()
        maps_up = int(ceil(self.__kwargs['time'] / (60 * map_time)))
        maps_down = int(floor(self.__kwargs['time'] / (60 * map_time)))
        if maps_up == 1 or maps_up <= 0 or maps_down <= 0:
            result = ([1, round(map_time * 60, 2), round(map_rms * 1000, 2)],)
        elif maps_up == 2:
            rms_up = (1000 * map_rms) / sqrt(maps_up)
            time_up = map_time * maps_up * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, c)
        else:
            rms_up = (1000 * map_rms) / sqrt(maps_up)
            rms_down = (1000 * map_rms) / sqrt(maps_down)
            time_up = map_time * maps_up * 60
            time_down = map_time * maps_down * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000, 2)]
            b = [maps_down, round(time_down, 2), round(rms_down, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, b, c)
        return result

    def __calculate_mjy(self):
        """Obtención de datos para el modo 1 y unidad mJy.

        :rtype: tuple
        """
        map_time, map_rms = self.__calculations()
        maps_up = int(ceil(self.__kwargs['time'] / (60 * map_time)))
        maps_down = int(floor(self.__kwargs['time'] / (60 * map_time)))
        if maps_up == 1 or maps_up <= 0 or maps_down <= 0:
            result = ([1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)],)
        elif maps_up == 2:
            rms_up = self.TTF * ((1000 * map_rms) / sqrt(maps_up))
            time_up = map_time * maps_up * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, c)
        else:
            rms_up = self.TTF * ((1000 * map_rms) / sqrt(maps_up))
            rms_down = self.TTF * ((1000 * map_rms) / sqrt(maps_down))
            time_up = map_time * maps_up * 60
            time_down = map_time * maps_down * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)]
            b = [maps_down, round(time_down, 2), round(rms_down, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, b, c)
        return result

    def __calculate_temperature(self):
        """Obtención de datos para el modo 2 y unidad Temperature.

        :rtype: tuple
        """
        map_time, map_rms = self.__calculations()
        maps_up = int(ceil(((1000 * map_rms) / self.__kwargs['rms']) ** 2))
        maps_down = int(floor(((1000 * map_rms) / self.__kwargs['rms']) ** 2))
        if maps_up == 1 or maps_up <= 0 or maps_down <= 0:
            result = ([1, round(map_time * 60, 2), round(map_rms * 1000, 2)],)
        elif maps_up == 2:
            rms_up = map_rms * 1000 / sqrt(maps_up)
            time_up = map_time * maps_up * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, c)
        else:
            rms_up = map_rms * 1000 / sqrt(maps_up)
            rms_down = map_rms * 1000 / sqrt(maps_down)
            time_up = map_time * maps_up * 60
            time_down = map_time * maps_down * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000, 2)]
            b = [maps_down, round(time_down, 2), round(rms_down, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, b, c)
        return result

    def __calculate_flux(self):
        """Obtención de datos para el modo 2 y unidad Flux.

        :rtype: tuple
        """
        map_time, map_rms = self.__calculations()
        maps_up = int(ceil(((1000 * map_rms * self.TTF) / self.__kwargs['rms']) ** 2))
        maps_down = int(floor(((1000 * map_rms * self.TTF) / self.__kwargs['rms']) ** 2))
        if maps_up == 1 or maps_up <= 0 or maps_down <= 0:
            result = ([1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)],)
        elif maps_up == 2:
            rms_up = map_rms * 1000 * self.TTF / sqrt(maps_up)
            time_up = map_time * maps_up * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, c)
        else:
            rms_up = map_rms * 1000 * self.TTF / sqrt(maps_up)
            rms_down = map_rms * 1000 * self.TTF / sqrt(maps_down)
            time_up = map_time * maps_up * 60
            time_down = map_time * maps_down * 60
            a = [1, round(map_time * 60, 2), round(map_rms * 1000 * self.TTF, 2)]
            b = [maps_down, round(time_down, 2), round(rms_down, 2)]
            c = [maps_up, round(time_up, 2), round(rms_up, 2)]
            result = (a, b, c)
        return result

    def calculate(self, **kwargs):
        """Función principal de la calculadora.

        Se encarga de devolver una tupla con los resultados.

        :Parameters:

        - central_frequency (int) - Frecuencia en GHz (85 - 115)
        - spectral_mode (dict) {'b': [value], 'ch': [value]}
        - mapx (int) - arcsec
        - mapy = (int) - arcsec
        - scan_speed (int) - arcsec/sec
        - time (int) - min
        - rms (float) - mK o mJy
        - units (str) - Tipo de unidad ('mK', 'mJy', 'Temperature', 'Flux')

        :rtype: tuple
        """
        self.__kwargs = kwargs
        if self.__mode == 1:
            if self.__kwargs['units'] == 'mK':
                result = self.__calculate_mk()
            elif self.__kwargs['units'] == 'mJy':
                result = self.__calculate_mjy()
        elif self.__mode == 2:
            if self.__kwargs['units'] == 'Temperature':
                result = self.__calculate_temperature()
            elif self.__kwargs['units'] == 'Flux':
                result = self.__calculate_flux()
        return result
